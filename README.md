Instalação:
- Clone Repository
- Install CocoaPods
- Just: pod install
- Open: My Documents.xcworkspace

App Functions:
- Autenticação de Usuários
- Cadastro de Usuário
- Upload Documento
- Adicionar Titulo Documento
- Ver todos os documentos
- Ver Detalhe de Documento
- Deletar Documento
- Upload pela galeria
- Compartilhar Documento

- Stack - Ambiente de Produção
- Linguagem App: Swift
- WebService: Alamofire
- Design Pattern: Model View Presenter (MVP)

TODO:
- Documentação
- Testes
- Perfil de Usuário
- Filtrar por Nome
- Formatar data
