//
//  UIView+Extensions.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func round(corners: UIRectCorner, with cornerRadius: Int = 10, andSetBackgroundColor color: UIColor = .white) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        
        self.layer.backgroundColor = color.cgColor
        self.layer.mask = rectShape
    }
    
    func addRoundedBorder() {
        layer.cornerRadius = frame.height / 2
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var roundedByHeight: Bool {
        get {
            return self.layer.cornerRadius == self.frame.height
        }
        set {
            if newValue == true {
                self.layer.cornerRadius = self.frame.height / 8
                //self.layer.masksToBounds = true
            } else {
                self.layer.cornerRadius = 0
            }
        }
    }
    
    @IBInspectable var roundedCircle: Bool {
        get {
            return self.layer.cornerRadius == self.frame.height / 2
        }
        set {
            if newValue == true {
                self.layer.cornerRadius = self.frame.height / 2
            } else {
                self.layer.cornerRadius = 0
            }
        }
    }
}
