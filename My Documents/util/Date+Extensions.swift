//
//  Date+Extensions.swift
//  My Documents
//
//  Created by Willian Pinho on 15/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
