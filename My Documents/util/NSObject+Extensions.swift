//
//  NSObject+Extensions.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

extension NSObject {
    class func className() -> String {
        return String(describing: self)
    }
}
