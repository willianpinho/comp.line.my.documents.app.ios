//
//  Formatter+Extensions.swift
//  My Documents
//
//  Created by Willian Pinho on 15/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

extension Formatter {
    static let iso8601: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return formatter
    }()
}
