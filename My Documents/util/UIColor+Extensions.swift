//
//  UIColor+Extensions.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class var myDocumentsOrange: UIColor {
        return UIColor(red: 0.925, green: 0.486, blue: 0.224, alpha: 1.00)
    }
    class var myDocumentsBlue: UIColor {
        return UIColor(red: 0.184, green: 0.267, blue: 0.596, alpha: 1.00)
    }
    
    class var myDocumentsYellow: UIColor {
        return UIColor(red: 1.000, green: 0.839, blue: 0.533, alpha: 1.00)
    }
    
    class var myDocumentsRed: UIColor {
        return UIColor(red: 1.000, green: 0.365, blue: 0.392, alpha: 1.00)

    }

    class var myDocumentsGreen: UIColor {
        return UIColor(red: 0.769, green: 0.902, blue: 0.502, alpha: 1.00)
    }

}
