//
//  Alert.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

import UIKit

class Alert {
    static func showMessage(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Ok", style: .default) { (alertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okButton)
        return alert
    }
}
