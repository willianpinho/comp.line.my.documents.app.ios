//
//  Document.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper
import ObjectMapperAdditions

class Document: Mappable {
    var id: String?
    var title: String?
    var url: String?
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        url <- map["url"]
        createdAt <- (map["createdAt"], DateTransform())
        updatedAt <- (map["updatedAt"], DateTransform())
        deletedAt <- (map["deletedAt"], DateTransform())
    }
}
