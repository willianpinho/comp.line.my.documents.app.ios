//
//  User.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper
import ObjectMapperAdditions

class User: Mappable {
    var id: String?
    var name: String?
    var email: String?
    var phone: String?
    var imageUrl: String?
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    var documents: [Document]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        imageUrl <- map["imageUrl"]
        createdAt <- (map["createdAt"], DateTransform())
        updatedAt <- (map["updatedAt"], DateTransform())
        deletedAt <- (map["deletedAt"], DateTransform())
        documents <- map["document"]
    }
}
