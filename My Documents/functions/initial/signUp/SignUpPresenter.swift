//
//  SignUpPresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

protocol SignUpView: NSObjectProtocol {
    func customLayout()
    func openCameraHandler()
    func openMainApp()
    func signUpUser()
}

class SignUpPresenter: NSObject {
    var view : SignUpView?
    
    func setViewDelegate(view: SignUpView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func openCameraHandler() {
        view?.openCameraHandler()
    }
    
    func openMainApp() {
        view?.openMainApp()
    }
    
    func signUpUser() {
        view?.signUpUser()
    }
    
    func loadUser(name: String, email: String, phone: String, image: UIImage, completion: @escaping (_ success: Bool?, _ message: String?, _ user: User?) -> Void) {
        HUD.show(.progress)

        UserService.uploadImage(image: image) { (success, message, url) in
            PKHUD.sharedHUD.show()
            if success {
                UserService.signUpUser(name: name, email: email, phone: phone, imageUrl: url!, completion: { (success, message, user) in
                    if success {
                        completion(true, nil, user!)
                        PKHUD.sharedHUD.hide()
                    } else {
                        completion(false, message!, nil)
                        PKHUD.sharedHUD.hide()
                    }
                })
                PKHUD.sharedHUD.hide()
            } else {
                completion(false, message!, nil)
                PKHUD.sharedHUD.hide()
            }

        }
        
    }
}
