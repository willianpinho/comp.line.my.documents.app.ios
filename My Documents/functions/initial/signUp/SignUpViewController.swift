//
//  SignUpViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    
    var textFields: [UITextField] = []

    var name: String?
    var email: String?
    var phone: String?
    var image: UIImage?

    var presenter = SignUpPresenter()
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
    }
    
    @objc func showActionSheet() {
        presenter.openCameraHandler()
    }
    
    @objc func signUp() {
        presenter.signUpUser()
    }
    
    @objc func goToMainApp() {
        presenter.openMainApp()
    }
    
    private func getAllTextFields() {
        nameTF.delegate = self
        emailTF.delegate = self
        phoneTF.delegate = self
        
        textFields.append(nameTF)
        textFields.append(emailTF)
        textFields.append(phoneTF)
    }
    
    func addActionButtons() {
        photoButton.addTarget(self, action: #selector(showActionSheet), for: UIControlEvents.touchUpInside)
        signUpButton.addTarget(self, action: #selector(signUpUser), for: UIControlEvents.touchUpInside)
    }
}

extension SignUpViewController: SignUpView {
    func customLayout() {
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigation(title: "Cadastrar", withTintColor: .white, barTintColor: .myDocumentsBlue, andAttributes: [NSAttributedStringKey.foregroundColor: UIColor.white], prefersLargeTitles: false)

        self.addActionButtons()

        getAllTextFields()
        self.removeBackButtonTitle()
    }
    
    func openCameraHandler() {
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.photoButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            self.photoButton.setImage(image, for: UIControlState.normal)
            self.photoButton.layoutIfNeeded()
        }
    }
    
    func openMainApp() {
        self.present(MainViewController.initializeTabBarController(), animated: true, completion: nil)
    }
    
    func showMessage(title: String, message: String) {
        self.present(Alert.showMessage(title: title, message: message), animated: true, completion: nil)
    }
    
    
    @objc func signUpUser() {
        if emailTF.text == "", nameTF.text == "", phoneTF.text == "" {
            return  self.present(Alert.showMessage(title: "Erro", message: "Você precisa preencher todos os campos"), animated: true, completion: nil)
        }
        self.name = self.nameTF.text
        self.email = self.emailTF.text
        self.phone = self.phoneTF.text
        self.image = self.photoButton.image(for: UIControlState.normal)

        if let name = name, let email = email, let phone = phone, let image = image {
            presenter.loadUser(name: name, email: email, phone: phone, image: image) { (success, message, user) in
                if success! {
                    self.presenter.openMainApp()
                } else {
                    self.showMessage(title: "ops", message: message!)
                }
            }
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
