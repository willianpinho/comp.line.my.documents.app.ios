//
//  SignInViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    var presenter = SignInPresenter()
    
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    var textFields: [UITextField] = []
    
    var email: String?
    var phone: String?
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
    }
    
    @objc func goToMainApp() {
        presenter.openMainApp()
    }
    
    @objc func signIn() {
        presenter.signInUser()
    }
    
    private func getAllTextFields() {
        emailTF.delegate = self
        phoneTF.delegate = self
        
        textFields.append(emailTF)
        textFields.append(phoneTF)
    }
    
    func addActionButtons() {
        signInButton.addTarget(self, action: #selector(signIn), for: UIControlEvents.touchUpInside)

    }
}

extension SignInViewController: SignInView {
    func customLayout() {
        self.hideNavigationBar(hidden: false)
        self.setNavigation(title: "Entrar", withTintColor: .white, barTintColor: .myDocumentsOrange, andAttributes: [NSAttributedStringKey.foregroundColor: UIColor.white], prefersLargeTitles: false)
        self.addActionButtons()
        getAllTextFields()
        self.removeBackButtonTitle()
    }
    
    func openMainApp() {
        self.present(MainViewController.initializeTabBarController(), animated: true, completion: nil)
    }
    
    func showMessage(title: String, message: String) {
        self.present(Alert.showMessage(title: title, message: message), animated: true, completion: nil)
    }
    
    func signInUser() {
        self.email = self.emailTF.text
        self.phone = self.phoneTF.text
        
        if let email = email, let phone = phone {
            presenter.loginUser(email: email, phone: phone) { (success, message, user) in
                if success! {
                    UserService.saveUser(userId: (user?.id)!)
                    self.presenter.openMainApp()
                } else {
                    self.showMessage(title: "ops", message: message!)
                }
            }
        }
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
