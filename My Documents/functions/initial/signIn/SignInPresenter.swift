//
//  SignInPresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import PKHUD

protocol SignInView: NSObjectProtocol {
    func customLayout()
    func openMainApp()
    func signInUser()
    func showMessage(title: String, message: String)
}

class SignInPresenter: NSObject {
    var view : SignInView?
    
    func setViewDelegate(view: SignInView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func openMainApp() {
        view?.openMainApp()
    }
    
    func signInUser() {
        view?.signInUser()
    }
    
    func showMessage(title: String, message: String) {
        view?.showMessage(title: title, message: message)
    }
    
    func loginUser(email: String, phone: String, completion: @escaping (_ success: Bool?, _ message: String?, _ user: User?) -> Void) {
        HUD.show(.progress)
        
        UserService.loginUser(email: email, phone: phone) { (success, message, user) in
            DispatchQueue.main.async {
                PKHUD.sharedHUD.show()
                if success {
                    completion(true, nil, user!)
                } else {
                    completion(false, message!, nil)
                }
                PKHUD.sharedHUD.hide()
            }
        }
    }
    
}
