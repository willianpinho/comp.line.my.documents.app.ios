//
//  InitialPresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

protocol InitialView: NSObjectProtocol {
    func customLayout()
    func openSignIn()
    func openSignUp()
    func openMainApp()
}

class InitialPresenter: NSObject {
    var view : InitialView?

    func setViewDelegate(view: InitialView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func goToSignIn() {
        view?.openSignIn()
    }
    
    func goToSignUp() {
        view?.openSignUp()
    }
    
    func goToMainApp() {
        view?.openMainApp()
    }
}
