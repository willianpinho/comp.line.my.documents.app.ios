//
//  InitialViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    var presenter = InitialPresenter()
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
        verifyLoggedUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
    }
    
    @objc func goToSignIn() {
        presenter.goToSignIn()
    }
    
    @objc func goToSignUp() {
        presenter.goToSignUp()
    }
    
    @objc func goToMainApp() {
        presenter.goToMainApp()
    }
    
    func verifyLoggedUser() {
        if UserService.currentUser() == true {
            presenter.goToMainApp()
        }
    }
   
}

extension InitialViewController: InitialView {
    func customLayout() {
        self.removeNavBarLine()
        self.removeBackButton()
        self.navigationController?.isNavigationBarHidden = true
        
        signInButton.addTarget(self, action: #selector(goToSignIn), for: UIControlEvents.touchUpInside)
        signUpButton.addTarget(self, action: #selector(goToSignUp), for: UIControlEvents.touchUpInside)
    }
    
    func openSignIn() {
        self.openViewControllerWithIdentifier(storyBoard: "Initial", identifier: "SignInViewController", animated: false)
    }
    
    func openSignUp() {
        self.openViewControllerWithIdentifier(storyBoard: "Initial", identifier: "SignUpViewController", animated: false)
    }
    
    func openMainApp() {
        self.present(MainViewController.initializeTabBarController(), animated: true, completion: nil)
    }
}
