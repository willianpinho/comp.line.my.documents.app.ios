//
//  FilePresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import PKHUD

protocol FileView: NSObjectProtocol {
    func customLayout()
    
}

class FilePresenter: NSObject {
    var view : FileView?
    
    func setViewDelegate(view: FileView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func deleteDocumentFromUser(document: Document, completion: @escaping (_ success: Bool, _ message: String?) -> Void) {
        HUD.show(.progress)

        if let userId = UserService.currentUserId() {
            UserService.deleteDocument(userId: userId, document: document) { (success, message) in
                PKHUD.sharedHUD.show()
                if success {
                    completion(true, nil)
                    PKHUD.sharedHUD.hide()
                } else {
                    completion(false, message!)
                    PKHUD.sharedHUD.hide()
                }
            }
        }
        
    }
}
