//
//  FileViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import Kingfisher

class FileViewController: UIViewController {
    
    @IBOutlet weak var imageSelected: UIImageView!
    
    var document: Document?
    var menuButton: UIBarButtonItem?

    var presenter = FilePresenter()
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
        if let document = document {
            if let urlImage = document.url {
                imageSelected.kf.setImage(with: URL(string: urlImage))
            }
        }
    }
    
    func deleteFile() {
        if let document = document {
            presenter.deleteDocumentFromUser(document: document) { (success, message) in
                if success {
                    self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.showMessage(title: "Ops", message: "Erro ao deletar o arquivo")
                    self.dismiss(animated: true, completion: nil)
                }
            }

        }
    }
    @objc func openMenu() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Compartilhar", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.shareFile()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Excluir", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.deleteFile()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func shareFile() {
        let activityViewController = UIActivityViewController(activityItems: [imageSelected.image], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = []
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}

extension FileViewController: FileView {
    func customLayout() {
        self.hideNavigationBar(hidden: false)
        self.setNavigation(title: "Detalhes", withTintColor: .white, barTintColor: .myDocumentsRed, andAttributes: [NSAttributedStringKey.foregroundColor: UIColor.white], prefersLargeTitles: false)
        self.removeBackButtonTitle()
        self.addActionOnButtons()
    }
    
    func addActionOnButtons() {
        menuButton = UIBarButtonItem(image: UIImage(named: "menu"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(openMenu))
        navigationItem.rightBarButtonItem = menuButton
    }
    
    func showMessage(title: String, message: String) {
        self.present(Alert.showMessage(title: title, message: message), animated: true, completion: nil)
    }
}
