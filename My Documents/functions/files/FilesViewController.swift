//
//  FilesViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import Kingfisher
import PKHUD

class FilesViewController: UIViewController {

    var presenter = FilesPresenter()
    
    @IBOutlet weak var tableView: UITableView!
    var cells: [UITableViewCell] = []
    var documents: [Document] = []
    
    private let refreshControl = UIRefreshControl()

    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
        setupTableView(tableView: tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
        getFilesFromUser()
    }
    
    func getFilesFromUser() {
        if let userId = UserService.currentUserId() {
            presenter.fetchFilesFromUser(userId: userId) { (success, message, documents) in
                if success! {
                    DispatchQueue.main.async {
                        self.documents = documents!
                        self.presenter.view?.reloadTableView()
                        self.refreshControl.endRefreshing()
                    }
                } else {
                    self.showMessage(title: "Erro", message: "Can't fetch Files")
                }
            }
            
        }
       
    }
    
    func openFileViewController(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Files", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "FileViewController") as? FileViewController {
            vc.document = documents[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc private func refreshTableViewData(_ sender: Any) {
        getFilesFromUser()
    }
}

extension FilesViewController: FilesView {
    func customLayout() {
        self.hideNavigationBar(hidden: false)
        self.setNavigation(title: "Arquivos", withTintColor: .white, barTintColor: .myDocumentsRed, andAttributes: [NSAttributedStringKey.foregroundColor: UIColor.white], prefersLargeTitles: false)
        self.removeBackButtonTitle()
        self.removeBackButton()
        
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
    }
    
    func showMessage(title: String, message: String) {
        self.present(Alert.showMessage(title: title, message: message), animated: true, completion: nil)
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
}

extension FilesViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            FileTableViewCell.self,
            ])
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableViewCell", for: indexPath) as! FileTableViewCell
        let document = documents[indexPath.row]
        cell.titleLabel.text = document.title
        
        let dateString = Formatter.iso8601.string(from: document.createdAt!)
        cell.createdLabel.text = dateString


        if let urlImage = document.url {
            cell.documentImageView.kf.setImage(with: URL(string: urlImage))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openFileViewController(indexPath: indexPath)
    }
}
