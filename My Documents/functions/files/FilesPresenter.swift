//
//  FilesPresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import PKHUD

protocol FilesView: NSObjectProtocol {
    func customLayout()
    func showMessage(title: String, message: String)
    func reloadTableView()
}

class FilesPresenter: NSObject {
    var view : FilesView?
    
    func setViewDelegate(view: FilesView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func fetchFilesFromUser(userId: String, completion: @escaping (_ success: Bool?, _ message: String?, _ documents: [Document]?) -> Void) {
        HUD.show(.progress)
        
        UserService.fetchDocuments(userId: userId) { (success, message, documents) in
            DispatchQueue.main.async {
                PKHUD.sharedHUD.show()
                if success {
                    completion(true, nil, documents!)
                } else {
                    completion(false, message!, nil)
                }
                PKHUD.sharedHUD.hide()
            }
        }
    }
}
