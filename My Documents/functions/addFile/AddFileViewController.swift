//
//  AddFileViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class AddFileViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleTF: UITextField!
    
    var imageSelected: UIImage?
    var presenter = AddFilePresenter()
    
    var uploadButton: UIBarButtonItem?

    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
        titleTF.delegate = self
        titleTF.becomeFirstResponder()
    }

    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
        self.image.image = imageSelected
    }
    
    @objc func uploadFile() {
        self.view.endEditing(true)
        
        if let imageSelected = imageSelected {
            presenter.uploadDocument(image: imageSelected) { (success, message, url) in
                if success! {
                    self.createDocument(url: url!)
                } else {
                    self.presenter.view?.showMessage(title: "Ops!", message: "Erro ao gerar o arquivo")
                }
            }
        }
        
    }
}

extension AddFileViewController: AddFileView {
    func createDocument(url: String) {
        presenter.createDocument(title: self.titleTF.text!, url: url, completion: { (success, message, document) in
            if success! {
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showMessage(title: "Ops", message: "Deu ruim, tenta de novo")
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    
    func customLayout() {
        self.hideNavigationBar(hidden: false)
        self.setNavigation(title: "Adicionar Arquivo", withTintColor: .white, barTintColor: .myDocumentsYellow, andAttributes: [NSAttributedStringKey.foregroundColor: UIColor.white], prefersLargeTitles: false)
        self.removeBackButtonTitle()
        self.addActionOnButtons()
    }
    
    func addActionOnButtons() {
        uploadButton = UIBarButtonItem(title: "Upload", style: UIBarButtonItemStyle.plain, target: self, action: #selector(uploadFile))
        uploadButton?.isEnabled = false
        navigationItem.rightBarButtonItem = uploadButton
    }
}

extension AddFileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text != "", textField.text?.isEmpty == false {
            guard let button = self.uploadButton else {
                return false
            }
            button.isEnabled = true
        }
        
        return true
    }
    
    func showMessage(title: String, message: String) {
        self.present(Alert.showMessage(title: title, message: message), animated: true, completion: nil)
    }
}
