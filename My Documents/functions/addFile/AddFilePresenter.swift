//
//  AddFilePresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

protocol AddFileView: NSObjectProtocol {
    func customLayout()
    func uploadFile()
    func createDocument(url: String)
    func showMessage(title: String, message: String)
}

class AddFilePresenter: NSObject {
    var view : AddFileView?
    
    func setViewDelegate(view: AddFileView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func uploadDocument(image: UIImage, completion: @escaping (_ success: Bool?, _ message: String?, _ url: String?) -> Void) {
        HUD.show(.progress)
        UserService.uploadImage(image: image) { (success, message, url) in
            PKHUD.sharedHUD.show()
            if success {
                completion(true, nil, url!)
                PKHUD.sharedHUD.hide()
            } else {
                completion(false, message!, nil)
                PKHUD.sharedHUD.hide()
            }
        }
    }
    
    func createDocument(title: String, url: String, completion: @escaping (_ success: Bool?, _ message: String?, _ document: Document?) -> Void) {
        HUD.show(.progress)
        if let userId = UserService.currentUserId() {
            UserService.createDocument(title: title, url: url, userId: userId) { (success, message, document) in
                PKHUD.sharedHUD.show()
                if success {
                    completion(true, nil, document)
                    PKHUD.sharedHUD.hide()
                } else {
                    completion(false, message!, nil)
                    PKHUD.sharedHUD.hide()
                }
            }
        }
    }
}
