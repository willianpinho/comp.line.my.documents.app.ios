//
//  ProfilePresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

protocol ProfileView: NSObjectProtocol {
    func customLayout()
}

class ProfilePresenter: NSObject {
    var view : ProfileView?
    
    func setViewDelegate(view: ProfileView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
}
