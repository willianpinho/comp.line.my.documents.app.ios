//
//  ProfileViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter = ProfilePresenter()
    var cells: [UITableViewCell] = []

    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
        setupTableView(tableView: tableView)
        generateCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.customLayout()
    }
    
    func generateCells() {
//        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as? ProfileHeaderTableViewCell {
//            cells.append(cell)
//        }
        
        tableView.reloadData()
        tableView.layoutIfNeeded()
    }
}

extension ProfileViewController: ProfileView {
    func customLayout() {
        self.hideNavigationBar(hidden: false)
        self.setNavigation(title: "Perfil", withTintColor: .white, barTintColor: .myDocumentsGreen, andAttributes: [NSAttributedStringKey.foregroundColor: UIColor.white], prefersLargeTitles: false)
        self.removeBackButton()
        self.removeNavigationShadow()
        self.removeNavBarLine()
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            ProfileHeaderTableViewCell.self,
            ProfileTitleWithIconTableViewCell.self
            ])
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.section]
    }
}
