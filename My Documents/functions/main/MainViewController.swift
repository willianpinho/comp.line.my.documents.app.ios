//
//  MainViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelectdidSelect viewController: UIViewController) {
        
    }
    static func initializeTabBarController() -> UITabBarController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateInitialViewController() as! UITabBarController
        
        tabBarController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        tabBarController.tabBar.tintColor = UIColor.blue
        tabBarController.selectedIndex = 1
        
        tabBarController.tabBar.backgroundColor = UIColor.clear
        tabBarController.tabBar.isTranslucent = true
        tabBarController.tabBar.addBlurEffect()
        
        let filesStoryboard = UIStoryboard(name: "Files", bundle: nil)
        let filesNC = filesStoryboard.instantiateInitialViewController() as! UINavigationController
        let filesBarItem = UITabBarItem(title: "Arquivos", image: UIImage(named: "files"), selectedImage: UIImage(named: "files"))
        filesNC.tabBarItem = filesBarItem
        
        let scanStoryboard = UIStoryboard(name: "Scan", bundle: nil)
        let scanNC = scanStoryboard.instantiateInitialViewController() as! UINavigationController
        let scanBarItem = UITabBarItem(title: "Scan", image: UIImage(named: "camera"), selectedImage: UIImage(named: "camera"))
        scanNC.tabBarItem = scanBarItem
//
//        let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)
//        let profileNC = profileStoryboard.instantiateInitialViewController() as! UINavigationController
//        let profileBarItem = UITabBarItem(title: "Perfil", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
//        profileNC.tabBarItem = profileBarItem
        
        tabBarController.viewControllers = [filesNC, scanNC]
        
        return tabBarController
    }
}
