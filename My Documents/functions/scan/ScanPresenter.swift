//
//  ScanPresenter.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

protocol ScanView: NSObjectProtocol {
    func customLayout()
    func openCameraHandler()
    func openAddFile(image: UIImage)
}

class ScanPresenter: NSObject {
    var view : ScanView?
    
    func setViewDelegate(view: ScanView) {
        self.view = view
    }
    
    func customLayout() {
        view?.customLayout()
    }
    
    func openCameraHandler() {
        view?.openCameraHandler()
    }
    
    func openAddFile(image: UIImage) {
        view?.openAddFile(image: image)
    }
}
