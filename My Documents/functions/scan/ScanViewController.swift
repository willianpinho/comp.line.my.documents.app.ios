//
//  ScanViewController.swift
//  My Documents
//
//  Created by Willian Pinho on 13/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class ScanViewController: UIViewController {
    @IBOutlet weak var newDocumentButton: UIButton!
    
    var presenter = ScanPresenter()
    var image: UIImage?
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenterDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        newDocumentButton.addTarget(self, action: #selector(showActionSheet), for: UIControlEvents.touchUpInside)
        presenter.customLayout()
    }
    
    @objc func showActionSheet() {
        presenter.openCameraHandler()
    }
    
    @objc func goToAddFileViewController() {
        guard let image = self.image else {
            return print("Can't get image")
        }
        presenter.openAddFile(image: image)
    }
}

extension ScanViewController: ScanView {
    func openAddFile(image: UIImage) {
        let storyboard = UIStoryboard(name: "Scan", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "AddFileViewController") as? AddFileViewController {
            vc.imageSelected = image
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openCameraHandler() {
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.presenter.openAddFile(image: image)
        }
    }
    
    func customLayout() {
        self.hideNavigationBar(hidden: true)
        self.removeBackButton()
        self.removeNavigationShadow()
        self.removeNavBarLine()
    }
}
