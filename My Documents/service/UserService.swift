//
//  UserService.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import Cloudinary

import UIKit

class UserService {
    static let defaults = UserDefaults.standard
    
    static func currentUser() -> Bool? {
        guard defaults.object(forKey: "userId") != nil else {
            return false
        }
        return true
    }
    
    static func currentUserId() -> String? {
        return defaults.object(forKey: "userId") as? String
    }
    
    static func saveUser(userId: String) {
        defaults.set("\(userId)", forKey: "userId")
    }
    
    static func removeUser(userId: String) {
        defaults.removeObject(forKey: "userId")
    }
    
    static func uploadImage(image: UIImage, completion: @escaping (_ success: Bool, _ message: String?, _ response: String?) -> Void) {
        guard let resizedImage = image.resized(withPercentage: 0.5) else {
            return completion(false, APIConstants.kMessageServerUnavailable, nil)
        }
        
        guard let configuration = CLDConfiguration(cloudinaryUrl: APIConstants.cloudinary) else {
            return completion(false, APIConstants.kMessageServerUnavailable, nil)
        }
        
        guard let imageData = UIImagePNGRepresentation(resizedImage) else {
            return completion(false, APIConstants.kMessageServerUnavailable, nil)
        }

        let cloudinary = CLDCloudinary(configuration: configuration)
        
        let params = CLDUploadRequestParams.init(params: ["resource_type": "image" as AnyObject])

        cloudinary.createUploader().upload(data: imageData, uploadPreset: "mwwibvuh", params: params, progress: { (progress) in
            print(progress)
        }) { (result, error) in
            guard let result = result?.url else {
                return completion(false, error?.localizedDescription, nil)
            }
            
            completion(true, nil, result)
        }
    }
    
    static func signUpUser(name: String, email: String, phone: String, imageUrl: String, completion: @escaping (_ success: Bool, _ message: String?, _ response: User?) -> Void) {
        var parameters:Parameters = [String : Any]()
        parameters["name"] = name
        parameters["email"] = email
        parameters["phone"] = phone
        parameters["imageUrl"] = imageUrl

        let url = APIConstants.users
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<User>) in
            if response.result.isFailure {
                completion(false, APIConstants.kMessageNoUser, nil)
            } else {
                completion(true, nil, response.result.value)
            }
        }

    }
    
    static func loginUser(email: String, phone: String, completion: @escaping (_ success: Bool, _ message: String?, _ response: User?) -> Void) {
        var parameters:Parameters = [String : Any]()
        parameters["email"] = email
        parameters["phone"] = phone
        let url = "https://my-documents-server.azurewebsites.net/users/login"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<User>) in
            if response.result.isFailure {
                completion(false, APIConstants.kMessageNoUser, nil)
            } else {
                completion(true, nil, response.result.value)
            }
        }
    }
    
    static func createDocument(title: String, url: String, userId: String, completion: @escaping (_ success: Bool, _ message: String?, _ document: Document?) -> Void) {
        var parameters:Parameters = [String : Any]()
        parameters["title"] = title
        parameters["url"] = url
        
        let url = "https://my-documents-server.azurewebsites.net/users/\(userId)/documents"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<Document>) in
            if response.result.isFailure {
                completion(false, APIConstants.kMessageServerUnavailable, nil)
            } else {
                completion(true, nil, response.result.value)
            }
        }
    }
    
    static func fetchDocuments(userId: String, completion: @escaping (_ success: Bool, _ message: String?, _ documents: [Document]?) -> Void) {
        let url = "https://my-documents-server.azurewebsites.net/users/\(userId)/documents"

        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseArray { (response: DataResponse<[Document]>) in
            if response.result.isFailure {
                completion(false, APIConstants.kMessageServerUnavailable, nil)
            } else {
                completion(true, nil, response.result.value)
            }
        }
    }
    
    static func deleteDocument(userId: String, document: Document, completion: @escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = "https://my-documents-server.azurewebsites.net/users/\(userId)/documents/\(document.id!)"
        Alamofire.request(url, method: .delete, encoding: JSONEncoding.default).responseArray { (response: DataResponse<[Document]>) in
            if response.response?.statusCode == 204 {
                completion(true, nil)
            } else {
                completion(false, APIConstants.kMessageNetworkError)
            }
        }
        
    }
}
