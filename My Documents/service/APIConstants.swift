//
//  APIConstants.swift
//  My Documents
//
//  Created by Willian Pinho on 14/04/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

struct APIConstants {
    static let kTimeOutPeriod:TimeInterval = 10
    static let kKeyTokenHeader = "token"
    static let kPage = "page"
    static let kKeyStatus = "status"
    static let kKeyMessage = "message"
    static let kKeyData = "data"
    static let kMessageNetworkError = "Sem conexão com a Internet."
    static let kMessageServerUnavailable = "Servidor Indisponível. Tente novamente mais tarde."
    static let kMessageIncorrectNumber = "Número de Telefone Incorreto"
    static let kMessageNoUser = "Usuário inexistente, crie seu cadastro"
    static let kMessageError = "Erro"
    
    static let formatDate = "dd-MM-yyyy HH:mm"
    static let cloudinary = "cloudinary://167984525727788:ahuxH1MglhdZFXhUDeffd2ECoXI@willian-pinho?secure=true"
    static let host = "https://my-documents-server.azurewebsites.net"
    static let users = APIConstants.host + "/users"
    static let login = APIConstants.users + "/login"
}
